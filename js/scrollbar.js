(function($){
$.fn.scrollbar = function (options){
	var defaults = {
		speed:6,
		scrollbar: '.scrollbar',
		retractable:true,
		smooth:false
	}; 
	var o =$.extend({},defaults,options);
	return this.each(function(){
	
		var $this = $(this);
		
		var scrollbar = $(o.scrollbar);
		var scrollbar_height = scrollbar.outerHeight(true);
		var viewport_height = $this.parent().height();
		var content_moving_height = $this.outerHeight(true) - viewport_height;

		var top = parseFloat($this.position().top);// get current top value minus 'px'
		// $this.css(top) won't work in chrome
		var sdelta = 0;
		var scrollbar_moving_space = viewport_height - scrollbar_height;
		if(content_moving_height <= 0) scrollbar.hide();
		
		$.fn.scrollbar.refresh = function () {
			scrollbar_height = scrollbar.outerHeight(true);
			viewport_height = $this.parent().height();
			content_moving_height = $this.outerHeight(true) - viewport_height;
			top = 0;
			sdelta = 0;
			set_scrollbar_pos(sdelta);
			set_content_pos(top);
			scrollbar_moving_space = viewport_height - scrollbar_height;
			if(content_moving_height <= 0){ scrollbar.hide();}
			else {scrollbar.show();}
		}
		
		$this.mousewheel(function(e, delta){
			e.preventDefault();
			var up = delta > 0 ? true : false;
			top = top + delta*o.speed;
			
			if(up){//delta = 0.333
				top = top>0 ? 0 : top;
				set_content_pos(top);// top is something like 5, 2, till 0
				console.log('top '+ top);
				sdelta = cal_scrollbar_pos(top);
				set_scrollbar_pos(sdelta);
			}else{// delta = -0.333
				console.log(top);
				top = Math.abs(top) > content_moving_height ? eval('-'+content_moving_height) : top ;// top is negative (-322)
				set_content_pos(top);
				console.log('top '+ top);
				sdelta = cal_scrollbar_pos(top);
				set_scrollbar_pos(sdelta);
			}
		});
		scrollbar.mousedown(function(e){
			e.preventDefault();
			var y = get_scrollbar_pos();
			var lastMouseY	= e.pageY;
			var minMouseY	= e.pageY - y;
			var maxMouseY	= minMouseY + scrollbar_moving_space;
			
			$(document).mousemove(function(e){
				var ey	= e.pageY;
				var y = get_scrollbar_pos();
				ey = Math.max(ey, minMouseY);
				ey = Math.min(ey, maxMouseY);
				var ny = y + (ey - lastMouseY);
				set_scrollbar_pos(ny);
				set_content_pos(cal_content_pos(ny* (-1)));
				lastMouseY	= ey;
			});
			$(document).mouseup(function(e){
				$(document).unbind('mousemove');
				$(document).unbind('mouseup');
				top = parseFloat($this.css('top'));
			});
		});
		
		get_scrollbar_pos = function(){
			return parseInt(scrollbar.css('top'));
		};
		get_scrollbar_height = function () {
			return scrollbar.height();
		};
		move_scrollbar = function (pos) {
			//scrollbar.css({top:pos+'px'});
		};
		set_content_pos = function (top) {
			$this.css({top:top+'px'});
		};
		set_scrollbar_pos = function (sdelta) {
			scrollbar.css({top:Math.abs(sdelta) +'px'});
		};
		cal_scrollbar_pos = function (top) {
			return (viewport_height-scrollbar_height)*top/content_moving_height;
		};
		cal_content_pos = function (y) {
			return content_moving_height*y/(viewport_height-scrollbar_height);
		}
	});	
}
})(jQuery);